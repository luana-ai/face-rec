import os
import cv2
import file

cam = cv2.VideoCapture(0)
cam.set(3, 640) # set video width
cam.set(4, 480) # set video height
face_detector = cv2.CascadeClassifier("cascades/haarcascade_frontalface_default.xml")

# Initialize individual sampling face count
count = 0

# For each person, enter one numeric face id
print("\n" + "=" * 16 + "> Register new user <" + "=" * 16)
face_name = input("\n[+] Enter the name of user end press: ")
print("\n[INFO] Initializing face capture. Look the camera and wait...")
file.edit_json(face_name)

while True:
	ret, img = cam.read()
	img = cv2.flip(img, 1) # flip video image vertically
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	faces = face_detector.detectMultiScale(gray, 1.3, 5)

	for (x, y, w, h) in faces:
		count += 1
		cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

		# Save the captured image into the datasets folder
		cv2.imwrite("dataset/user-" + str(file.read_json().index(face_name)) + "-" + str(count) + ".jpg", gray[y: y + h, x: x + w])
		cv2.imshow("image", img)

	k = cv2.waitKey(100) & 0xff # Press "ESC" for exiting video
	if k == 27:
		break
	elif count >= 100: break # Take 100 face sample and stop video

# Do a bit of cleanup
print("\n[INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()