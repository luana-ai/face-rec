import sys
import file

file.new_folder("dataset")

print("\n" + "=" * 16 + "> Face Recognition <" + "=" * 16 + "\n")
print("Welcome" + "\n")

print(" [1 / new_user] => Register new user")
print(" [2 / training] => Training the faces of user")
print(" [3 / recognition] => Recognition the face of user")
print(" [4 / exit] => Exit the program")
action = input("\nSelect the action: ")

if (action == "1" or action == "new_user"):
	import dataset
elif (action == "2" or action == "training"):
	import training
elif (action == "3" or action == "recognition"):
	import recognition
elif (action == "4" or action == "exit"):
	sys.exit()