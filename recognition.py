import os
import cv2 
import json
import file
import numpy as np

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")
faceCascade = cv2.CascadeClassifier("cascades/haarcascade_frontalface_default.xml")

font = cv2.FONT_HERSHEY_SIMPLEX

#iniciate id counter
id = 0

# Initialize and start realtime video capture
cam = cv2.VideoCapture(0)
cam.set(3, 640) # set video widht
cam.set(4, 480) # set video height

# Define min window size to be recognized as a face
minW = 0.1 * cam.get(3)
minH = 0.1 * cam.get(4)

print("\n" + "=" * 21 + "> Results <" + "=" * 21 + "\n")
while True:
	ret, img = cam.read()
	img = cv2.flip(img, 1) # Flip vertically
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	faces = faceCascade.detectMultiScale( 
		gray,
		minNeighbors = 5,
		scaleFactor = 1.2,
		minSize = (int(minW), int(minH)),
	)

	for (x, y, w, h) in faces:
		cv2.rectangle(img, (x, y), (x + w,y + h), (0, 255, 0), 2)
		id, confidence = recognizer.predict(gray[y: y + h, x: x + w])

		# Check if confidence is less them 100 ==> "0" is perfect match 
		if (confidence < 50):
			id = file.read_json()[id]
			confidence = "{0}%".format(round(100 - confidence))
		else:
			id = "Unknown"
			confidence = "{0}%".format(round(100 - confidence))

		print(id + " => " + confidence)
		cv2.putText(img, str(id), (x + 10, y - 10), font, 0.6, (255, 255, 255), 2)
		cv2.putText(img, str(confidence), (x + 5, y + h - 10), font, 0.5, (0, 255, 255), 1)  
	
	cv2.imshow("Face Recognition", img) 

	k = cv2.waitKey(10) & 0xff # Press "ESC" for exiting video
	if k == 27:
		break

# Do a bit of cleanup
print("\n[INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()