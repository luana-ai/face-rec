## Face Recognition

Real-time face recognition project with OpenCV and Python

[Original repository](https://github.com/thecodacus/Face-Recognition)

[Real-Time Face Recognition: an End-to-End Project](https://www.hackster.io/mjrobot/real-time-face-recognition-an-end-to-end-project-a10826)