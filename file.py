import os
import json
users_db = 'users.json'

def read_json():
	with open('users.json', 'r', encoding='utf8') as f:
		return json.load(f)

def edit_json(user):
	with open('users.json') as f:
		content = json.load(f)

	content.append(user)

	with open('users.json', 'w', encoding='utf8') as f:
		json.dump(content, f, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ':'))

def write_json(users):
	with open('users.json', 'w', encoding='utf8') as f:
		json.dump(users, f, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ':'))

def new_folder(folder):
    if (os.path.isdir(folder) != True):
    	os.mkdir(folder)
    else:
        return folder